#include "perf.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <math.h>


int fddev = -1;

int64_t overhead = 0;
int64_t perf_results_list[PERF_NBR_MEASUREMENTS];

// Statistics functions and sorting
static int compare( const void* a, const void* b);
static int64_t get_median(int64_t* list, int length);
static double get_mean(int64_t* list, int length);
static double get_variance(int64_t* list, int length, double mean);


void perf_initialize(void)
{
	static struct perf_event_attr pe_attr;

	pe_attr.type = PERF_TYPE_HARDWARE;
	pe_attr.size = sizeof(struct perf_event_attr);
	pe_attr.config = PERF_COUNT_HW_CPU_CYCLES;
	pe_attr.disabled = 1;
#if ARCH == armv7l
	pe_attr.exclude_kernel = 0;
	pe_attr.exclude_hv = 0;
#elif ARCH == aarch64
	pe_attr.exclude_kernel = 1;
	pe_attr.exclude_hv = 1;
#else  // unknown ARCH

#endif  // ARCH
	pe_attr.exclusive = 1;
	pe_attr.exclude_user = 0;
	pe_attr.exclude_idle = 0;
	pe_attr.pinned = 1;
	pe_attr.exclude_callchain_kernel = 1;
	pe_attr.exclude_callchain_user = 1;

	fddev = syscall(__NR_perf_event_open, &pe_attr, 0, -1, -1, 0);

	if (fddev == -1) {
		fprintf(stderr, "Error in perf_initialize with config: %llx\n", pe_attr.config);
		exit(EXIT_FAILURE);
	}

	overhead = perf_get_overhead();

	printf("%s %ld\n", "Median overhead:", overhead);

}

int64_t perf_get_overhead(void)
{
	int64_t overhead_list[PERF_OVERHEAD_SAMPLE_SIZE];

	for(int i=0; i<PERF_OVERHEAD_SAMPLE_SIZE; i++){
		perf_start_measurement();
		perf_stop_measurement();
		overhead_list[i] = perf_get_cycles();
	}

	// sort list
	qsort( overhead_list, PERF_OVERHEAD_SAMPLE_SIZE, sizeof(int64_t), compare );

	return get_median(overhead_list, PERF_OVERHEAD_SAMPLE_SIZE);
}

inline void perf_start_measurement(void)
{
	ioctl(fddev, PERF_EVENT_IOC_RESET, 0);
	ioctl(fddev, PERF_EVENT_IOC_ENABLE, 0);
}

inline void perf_stop_measurement(void)
{
	ioctl(fddev, PERF_EVENT_IOC_DISABLE, 0);
}

void perf_terminate(void)
{
	close(fddev);
}

inline int64_t perf_get_cycles(void)
{
	int64_t result = 0;
	read(fddev, &result, sizeof(result));
	return result - overhead;
}

void perf_print_statistics_short(void)
{
	// sort list
	qsort( perf_results_list, PERF_NBR_MEASUREMENTS, sizeof(int64_t), compare );

	// print properties
	printf("%10ld", get_median(perf_results_list, PERF_NBR_MEASUREMENTS) );
	double mean;
	char buffer [20];
	mean = get_mean(perf_results_list, PERF_NBR_MEASUREMENTS);
	sprintf(buffer,"(%d)",(int)mean);
	printf(" %20s\n", buffer);
}


void perf_print_statistics(void)
{
    // sort list
    qsort( perf_results_list, PERF_NBR_MEASUREMENTS, sizeof(int64_t), compare );

    // print properties
    printf("%-30s %20ld\n", "Minimum:", perf_results_list[0]);
    printf("%-30s %20ld\n", "Maximum:", perf_results_list[PERF_NBR_MEASUREMENTS-1]);
    printf("%-30s %20ld\n", "Median:", get_median(perf_results_list, PERF_NBR_MEASUREMENTS) );
    double mean, variance;
    mean = get_mean(perf_results_list, PERF_NBR_MEASUREMENTS);
    variance = get_variance(perf_results_list, PERF_NBR_MEASUREMENTS, mean);
    printf("%-30s %20f\n", "Mean:",  mean);
    printf("%-30s %20f\n","Standard deviation:", sqrt(variance));
}

static int compare(const void * a, const void * b)
{
     int64_t int_a = * ( (int64_t*) a );
     int64_t int_b = * ( (int64_t*) b );

     if ( int_a == int_b ) return 0;
     else if ( int_a < int_b ) return -1;
     else return 1;
}

static int64_t get_median(int64_t* list, int length)
{
	// list needs to be sorted!
	if(length &(1)){
		return list[length/2];
	}else{
		return (list[length/2]+list[(length/2)-1]) / 2;
	}
}

static double get_mean(int64_t* list, int length)
{
  double sum = 0;
  for(int i = 0; i<length; i++){
    sum += (double)list[i];
  }
  return sum/(double)length;
}

static double get_variance(int64_t* list, int length, double mean)
{
  double sum = 0;
  for(int i = 0; i<length; i++){
    sum += pow( ((double)list[i] - mean), 2);
  }
  return sum/(double)length;
}
