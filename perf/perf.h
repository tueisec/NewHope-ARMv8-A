#ifndef _PERF_H_
#define _PERF_H_

#include <stdint.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>


// settings
#define PERF_OVERHEAD_SAMPLE_SIZE 65536
#define PERF_NBR_MEASUREMENTS 65536


// high level API
void perf_initialize(void);
void perf_terminate(void);

#define perf_measure(function) do{\
  for(int	i=0; i<PERF_NBR_MEASUREMENTS; i++){\
    perf_start_measurement();\
    function;\
    perf_stop_measurement();\
    /* store cycles needed */\
    perf_results_list[i] = perf_get_cycles();\
  }\
  perf_print_statistics_short();\
}while(0)

void perf_print_statistics(void);
void perf_print_statistics_short(void);


// low level API
extern int64_t perf_results_list[PERF_NBR_MEASUREMENTS];

int64_t perf_get_overhead(void);
void perf_start_measurement(void);
void perf_stop_measurement(void);
int64_t perf_get_cycles(void);


#endif // #ifndef _PERF_H_
