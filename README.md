# NewHope and NewHope-Simple Post-Quantum Key-Exchange Protocols for ARMv8-A processors

## Build

```shell
$ cd build
$ cmake ..
$ make
```

## Run

```shell
$ make verify
$ make testvectors
$ make test_statistical
```

## Performance

```shell
$ make speed
$ make size
```

## Further Notes

* This repository uses a public domain reference implementation of ChaCha20.

* A further speed-up can be achieved by using the NEON vectorized ChaCha20
implementation from
https://git.kernel.org/pub/scm/linux/kernel/git/ardb/linux.git/plain/arch/arm64/crypto/chacha20-neon-core.S?h=crypto-arm-v4.11
which is published under GPLv2.

## Reference

* S. Streit and F. De Santis, ''Post-Quantum Key Exchange on ARMv8-A -- A New Hope for NEON made Simple'', Cryptology ePrint Archive, Report 2017/388 ([pdf](https://eprint.iacr.org/2017/388.pdf))