/* 
 * Based on the public domain implementation taken from https://cryptojedi.org/crypto/data/newhope-20160815.tar.bz2
 * of newhope.c
 * by Erdem Alkim, Léo Ducas, Thomas Pöppelmann, and Peter Schwabe: Post-quantum key exchange - a new hope.
 * Proceedings of the 25th USENIX Security Symposium, USENIX Association (2016).
 */
 
#include "newhope-simple.h"
#include "poly.h"
#include "randombytes.h"
#include "error_correction.h"
#include "fips202.h"


static int16_t abs_int16(int16_t v)
{
  int16_t mask = v >> 15;
  return (v + mask) ^ mask;
}

void nhs_encode(poly *k, const unsigned char *v)
{
  // v points to 256 random bits
  // k has 1024 coefficients
  // output is fully reduced to mod q
  for(int i = 0; i<32; i++){
    for(int j = 0; j<8; j++){
      k->coeffs[8*i+j] = (v[i]>>j & 1) * PARAM_Q_HALF;
      k->coeffs[8*i+j+256] = k->coeffs[8*i+j];
      k->coeffs[8*i+j+512] = k->coeffs[8*i+j];
      k->coeffs[8*i+j+768] = k->coeffs[8*i+j];
    }
  }
}

void nhs_decode(unsigned char *v, const poly *k)
{
  // v points to 256 bits / 32bytes
  // k has 1024 coefficients
  // input needs to be fully reduced to mod q
  int16_t tmp;
  for(int i = 0; i<32; i++){
    for(int j = 0; j<8; j++){
      tmp  = abs_int16(k->coeffs[8*i+j    ] - PARAM_Q_HALF);
      tmp += abs_int16(k->coeffs[8*i+j+256] - PARAM_Q_HALF);
      tmp += abs_int16(k->coeffs[8*i+j+512] - PARAM_Q_HALF);
      tmp += abs_int16(k->coeffs[8*i+j+768] - PARAM_Q_HALF);

      // The following two lines do the same as this if statement
      // if (tmp < PARAM_Q){
      //   // v_i = 1
      //   v[i] |= 1 << j;
      // }else{
      //   // v_i = 0
      //   v[i] &= ~(1 << j);
      // }
      tmp -= PARAM_Q;
      v[i] ^= ( (tmp>>15) ^ v[i] ) & (1 << j);
    }
  }
}


void nhs_compress(unsigned char *comp, const poly *c)
{
  // c: 1024 uint16_t coeffs
  // comp: 384 bytes
  // input needs to be fully reduced to mod q
  uint32_t t[8], tmp;
  for(int i = 0; i<128; i++){
    for(int j = 0; j<8; j++){
      tmp = c->coeffs[8*i+j] * 8;  // result 17 bit, but < 98305

      // # rounding division by q

      // division by q/2
      tmp  = tmp * 43687;  // less than 32-bit for fully reduced inputs
      t[j] = tmp >> 28;

      // rounding division by q
      tmp  = t[j] & 1;
      t[j] = (t[j]>>1) + tmp;  // t[j] = round(x/PARAM_Q)

      t[j] = t[j] & 0x07;  // mod 8
    }
    // Merge resuls
    comp[3*i  ] = (t[0]     ) | (t[1] << 3) | (t[2] << 6);
    comp[3*i+1] = (t[2] >> 2) | (t[3] << 1) | (t[4] << 4) | (t[5] << 7);
    comp[3*i+2] = (t[5] >> 1) | (t[6] << 2) | (t[7] << 5);
  }
}

void nhs_decompress(poly *c, const unsigned char *comp)
{
  // c: 1024 uint16_t coeffs
  // comp: 384 bytes
  // output is fully reduced to mod q
  uint32_t t[8];
  for(int i = 0; i<128; i++){
    // Extract compressed values
    t[0] = ( (comp[3*i  ]     ) & 0x07 );
    t[1] = ( (comp[3*i  ] >> 3) & 0x07 );
    t[2] = ( (comp[3*i  ] >> 6) & 0x03 ) | ( (comp[3*i+1] & 0x01) << 2 );
    t[3] = ( (comp[3*i+1] >> 1) & 0x07 );
    t[4] = ( (comp[3*i+1] >> 4) & 0x07 );
    t[5] = ( (comp[3*i+1] >> 7) & 0x01 ) | ( (comp[3*i+2] & 0x03) << 1 );
    t[6] = ( (comp[3*i+2] >> 2) & 0x07 );
    t[7] = ( (comp[3*i+2] >> 5) & 0x07 );
    for(int j = 0; j<8; j++){
      t[j] = t[j] * PARAM_Q;  // result 17 bit!!
      t[j] = (t[j] + 4)>>3;  // rounding division by 8
      c->coeffs[8*i+j] = t[j];  // no need for mod q
    }
  }
}





static void encode_a(unsigned char *r, const poly *pk, const unsigned char *seed)
{
  int i;
  poly_tobytes(r, pk);
  for(i=0;i<NEWHOPE_SEEDBYTES;i++)
    r[POLY_BYTES+i] = seed[i];
}

static void decode_a(poly *pk, unsigned char *seed, const unsigned char *r)
{
  int i;
  poly_frombytes(pk, r);
  for(i=0;i<NEWHOPE_SEEDBYTES;i++)
    seed[i] = r[POLY_BYTES+i];
}

static void nhs_encode_b(unsigned char *r, const poly *b, const unsigned char *comp)
{
  int i;
  poly_tobytes(r,b);
  for(i=0;i<PARAM_N/8*3;i++)
    r[POLY_BYTES+i] = comp[i];
}

static void nhs_decode_b(poly *b, unsigned char *comp, const unsigned char *r)
{
  int i;
  poly_frombytes(b, r);
  for(i=0;i<PARAM_N/8*3;i++)
    comp[i] = r[POLY_BYTES+i];
}

static void gen_a(poly *a, const unsigned char *seed)
{
    poly_uniform(a,seed);
}


// API FUNCTIONS

void newhope_simple_keygen(unsigned char *send, poly *sk)
{
  // identical to newhope_keygen
  poly a, e, r, pk;
  unsigned char seed[NEWHOPE_SEEDBYTES];
  unsigned char noiseseed[32];

  randombytes(seed, NEWHOPE_SEEDBYTES);
  sha3256(seed, seed, NEWHOPE_SEEDBYTES); /* Don't send output of system RNG */
  randombytes(noiseseed, 32);

  gen_a(&a, seed);

  poly_getnoise(sk,noiseseed,0);
  poly_ntt(sk);

  poly_getnoise(&e,noiseseed,1);
  poly_ntt(&e);

  poly_pointwise(&r,sk,&a);
  poly_add(&pk,&e,&r);

  encode_a(send, &pk, seed);
}


void newhope_simple_sharedb(unsigned char *sharedkey, unsigned char *send, const unsigned char *received)
{
  //sharedkey:    32 bytes
  //send:       2176 bytes  -- different
  //received:   1824 bytes
  poly sp, ep, v, a, pka, c, epp, bp;
  unsigned char seed[NEWHOPE_SEEDBYTES];
  unsigned char noiseseed[32];
  // newhope-simple:
  unsigned char comp[384];


  randombytes(noiseseed, 32);

  decode_a(&pka, seed, received);
  gen_a(&a, seed);

  poly_getnoise(&sp,noiseseed,0);
  poly_ntt(&sp);
  poly_getnoise(&ep,noiseseed,1);
  poly_ntt(&ep);

  poly_pointwise(&bp, &a, &sp);
  poly_add(&bp, &bp, &ep);


  // different to newhope_sharedb after this point
  randombytes(sharedkey, 32);
  sha3256(sharedkey, sharedkey, 32);
  nhs_encode(&c, sharedkey);

  poly_pointwise(&v, &pka, &sp);
  poly_invntt(&v);

  poly_getnoise(&epp,noiseseed,2);
  poly_add(&v, &v, &epp);

  poly_add(&v, &v, &c);

  nhs_compress(comp, &v);

  nhs_encode_b(send, &bp, comp);

#ifndef STATISTICAL_TEST
  sha3256(sharedkey, sharedkey, 32);
#endif
}


void newhope_simple_shareda(unsigned char *sharedkey, const poly *sk, const unsigned char *received)
{
  //sharedkey:    32 bytes
  //sk:         1024 coeffs
  //received:   2176 bytes  -- different
  poly v,bp, c;
  // newhope-simple:
  unsigned char comp[384];

  nhs_decode_b(&bp, comp, received);
  nhs_decompress(&c, comp);

  poly_pointwise(&v,sk,&bp);
  poly_invntt(&v);

  poly_sub(&v, &c, &v);  // output is fully reduced

  nhs_decode(sharedkey, &v);  // input needs to be fully reduced

#ifndef STATISTICAL_TEST
  sha3256(sharedkey, sharedkey, 32);
#endif
}
