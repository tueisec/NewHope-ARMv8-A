/* 
 * Based on the public domain implementation
 * of poly.c
 * from https://cryptojedi.org/crypto/data/newhope-20160815.tar.bz2
 * by Erdem Alkim, Léo Ducas, Thomas Pöppelmann, and Peter Schwabe: Post-quantum key exchange - a new hope.
 * Proceedings of the 25th USENIX Security Symposium, USENIX Association (2016).
 */

#include "poly.h"
#include "ntt.h"
#include "randombytes.h"
// #include "reduce.h" -- removed in this version
#include "fips202.h"
#include "crypto_stream_chacha20.h"

void poly_frombytes(poly *r, const unsigned char *a)
{
  int i;
  for(i=0;i<PARAM_N/4;i++)
  {
    r->coeffs[4*i+0] =                               a[7*i+0]        | (((uint16_t)a[7*i+1] & 0x3f) << 8);
    r->coeffs[4*i+1] = (a[7*i+1] >> 6) | (((uint16_t)a[7*i+2]) << 2) | (((uint16_t)a[7*i+3] & 0x0f) << 10);
    r->coeffs[4*i+2] = (a[7*i+3] >> 4) | (((uint16_t)a[7*i+4]) << 4) | (((uint16_t)a[7*i+5] & 0x03) << 12);
    r->coeffs[4*i+3] = (a[7*i+5] >> 2) | (((uint16_t)a[7*i+6]) << 6);
  }
}

void poly_tobytes(unsigned char *r, const poly *p)
{
  int i;
  uint16_t t0,t1,t2,t3;
  for(i=0;i<PARAM_N/4;i++)
  {
    t0 = p->coeffs[4*i+0];
    t1 = p->coeffs[4*i+1];
    t2 = p->coeffs[4*i+2];
    t3 = p->coeffs[4*i+3];

    // further reduction not needed, as only output poly_add is used as input
    // and it is already fully reduced

    r[7*i+0] =  t0 & 0xff;
    r[7*i+1] = (t0 >> 8) | (t1 << 6);
    r[7*i+2] = (t1 >> 2);
    r[7*i+3] = (t1 >> 10) | (t2 << 4);
    r[7*i+4] = (t2 >> 4);
    r[7*i+5] = (t2 >> 12) | (t3 << 2);
    r[7*i+6] = (t3 >> 6);
  }
}

void poly_uniform(poly *a, const unsigned char *seed)
{
  unsigned int pos=0, ctr=0;
  uint16_t val;
  uint64_t state[25];
  unsigned int nblocks=14;
  uint8_t buf[SHAKE128_RATE*nblocks];

  shake128_absorb(state, seed, NEWHOPE_SEEDBYTES);

  shake128_squeezeblocks((unsigned char *) buf, nblocks, state);

  while(ctr < PARAM_N)
  {
    val = (buf[pos] | ((uint16_t) buf[pos+1] << 8));
    if(val < 5*PARAM_Q)
      a->coeffs[ctr++] = val;
    pos += 2;
    if(pos > SHAKE128_RATE*nblocks-2)
    {
      nblocks=1;
      shake128_squeezeblocks((unsigned char *) buf,nblocks,state);
      pos = 0;
    }
  }
}


void poly_ntt(poly *r)
{
  mul_coefficients(r->coeffs, psis_bitrev_montgomery);
  ntt((uint16_t *)r->coeffs, omegas_montgomery);
}

void poly_invntt(poly *r)
{
  bitrev_vector(r->coeffs);
  ntt((uint16_t *)r->coeffs, omegas_inv_montgomery);
  mul_coefficients(r->coeffs, psis_inv_montgomery);
}
