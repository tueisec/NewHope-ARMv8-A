#ifndef NEWHOPE_SIMPLE_H
#define NEWHOPE_SIMPLE_H

#include "poly.h"
#include <stdio.h>

#include "params.h"
#define PARAM_Q_HALF  6144
#define NEWHOPE_SIMPLE_SENDABYTES 1824
#define NEWHOPE_SIMPLE_SENDBBYTES 2176

#define NEWHOPE_SIMPLE_VERSION "aarch64"

void newhope_simple_keygen(unsigned char *send, poly *sk);
void newhope_simple_sharedb(unsigned char *sharedkey, unsigned char *send, const unsigned char *received);
void newhope_simple_shareda(unsigned char *sharedkey, const poly *ska, const unsigned char *received);


// poly: 1024 uint16_t coeffs
// v: 32 bytes
// comp: 384 bytes

void nhs_encode(poly *k, const unsigned char *v);

void nhs_decode(unsigned char *v, const poly *k);


void nhs_compress(unsigned char *comp, const poly *c);

void nhs_decompress(poly *c, const unsigned char *comp);

#endif
