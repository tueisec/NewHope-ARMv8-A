/*
 * Based on the public domain implementation taken from https://cryptojedi.org/crypto/data/newhope-20160815.tar.bz2
 * of test/test_newhope.c
 * by Erdem Alkim, Léo Ducas, Thomas Pöppelmann, and Peter Schwabe: Post-quantum key exchange - a new hope.
 * Proceedings of the 25th USENIX Security Symposium, USENIX Association (2016).
 */

#include <newhope-simple.h>
#include <poly.h>
#include <randombytes.h>
#include <crypto_stream_chacha20.h>
#include <error_correction.h>

#include <math.h>
#include <stdio.h>
#include <string.h>

#define NTESTS 1000

int compare_keys(poly *a, poly *b){

  int i;

  for(i=0; i<256; i++){
    if (a->coeffs[i] != b->coeffs[i]){
      return -1;
    }
  }
  return 0;
}


int test_keys(){
  poly sk_a;
  unsigned char key_a[32], key_b[32];
  unsigned char senda[NEWHOPE_SIMPLE_SENDABYTES];
  unsigned char sendb[NEWHOPE_SIMPLE_SENDBBYTES];
  int i;



  for(i=0; i<NTESTS; i++)
  {
    //Alice generates a public key
    newhope_simple_keygen(senda, &sk_a);

    //Bob derives a secret key and creates a response
    newhope_simple_sharedb(key_b, sendb, senda);

    //Alice uses Bobs response to get her secre key
    newhope_simple_shareda(key_a, &sk_a, sendb);

    if(memcmp(key_a, key_b, 32)){
      printf("ERROR keys in iteration: %d\n", i);
      for(int j = 0; j < 32; j++){
        printf("%d, %d\n", key_a[j], key_b[j]);
      }
      return 1;
    }
  }

  return 0;
}

int test_invalid_sk_a()
{
  poly sk_a;
  unsigned char key_a[32], key_b[32];
  unsigned char senda[NEWHOPE_SIMPLE_SENDABYTES];
  unsigned char sendb[NEWHOPE_SIMPLE_SENDBBYTES];
  unsigned char noiseseed[32];
  int i;

  randombytes(noiseseed,32);

  for(i=0; i<NTESTS; i++)
  {
    //Alice generates a public key
    newhope_simple_keygen(senda, &sk_a);

    //Bob derives a secret key and creates a response
    newhope_simple_sharedb(key_b, sendb, senda);

    //Overwrite the secret key
    poly_getnoise(&sk_a,noiseseed,i);

    //Alice uses Bobs response to get her secre key
    newhope_simple_shareda(key_a, &sk_a, sendb);

    if(!memcmp(key_a, key_b, 32)){
      printf("ERROR invalid sk_a in iteration: %d\n", i);
      return 1;
    }
  }
  return 0;
}


int test_invalid_ciphertext()
{
  poly sk_a;
  unsigned char key_a[32], key_b[32];
  unsigned char senda[NEWHOPE_SIMPLE_SENDABYTES];
  unsigned char sendb[NEWHOPE_SIMPLE_SENDBBYTES];
  int i;

  for(i=0; i<10; i++)
  {
    //Alice generates a public key
    newhope_simple_keygen(senda, &sk_a);

    //Bob derives a secret key and creates a response
    newhope_simple_sharedb(key_b, sendb, senda);

    //Change some byte in the "ciphertext"
    randombytes(sendb+42,1);

    //Alice uses Bobs response to get her secre key
    newhope_simple_shareda(key_a, &sk_a, sendb);

    if(!memcmp(key_a, key_b, 32)){
      printf("ERROR invalid sendb in iteration: %d\n", i);
      return 1;
    }
  }

  return 0;
}


int main(){
  int i = 0;
  printf("Running test_newhope_simple with version: %-30s\n", NEWHOPE_SIMPLE_VERSION);
  i += test_keys();
  i += test_invalid_sk_a();
  i += test_invalid_ciphertext();
  if(!i){
    printf("No errors found in test_newhope_simple.\n");
  }else
  return i;
}
