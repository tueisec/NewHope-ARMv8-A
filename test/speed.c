/*
 * Based on the public domain implementation taken from https://cryptojedi.org/crypto/data/newhope-20160815.tar.bz2
 * of test/speed.c
 * by Erdem Alkim, Léo Ducas, Thomas Pöppelmann, and Peter Schwabe: Post-quantum key exchange - a new hope.
 * Proceedings of the 25th USENIX Security Symposium, USENIX Association (2016).
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
// include performance measurement library
#include <perf.h>
// include newhope library
#include <newhope.h>
#include <ntt.h>
#include <poly.h>
#include <newhope-simple.h>



extern uint16_t omegas_montgomery[];
extern uint16_t omegas_inv_montgomery[];

extern uint16_t psis_bitrev_montgomery[];
extern uint16_t psis_inv_montgomery[];

// define variables needed for measurement
poly sk_a;
unsigned char key_a[32], key_b[32];
unsigned char senda[PERF_NBR_MEASUREMENTS*NEWHOPE_SENDABYTES];
unsigned char sendb[PERF_NBR_MEASUREMENTS*NEWHOPE_SENDBBYTES];
unsigned char seed[NEWHOPE_SEEDBYTES];

unsigned char sendas[PERF_NBR_MEASUREMENTS*NEWHOPE_SIMPLE_SENDABYTES];
unsigned char sendbs[PERF_NBR_MEASUREMENTS*NEWHOPE_SIMPLE_SENDBBYTES];

// poly: 1024 uint16_t coeffs
// v: 32 bytes
// comp: 384 bytes

unsigned char comp[384];

uint16_t coeffs[PARAM_N];
poly a,b,c,r,k;


// for crypto_stream_chacha20
uint32_t buf[PARAM_N];
unsigned char n[8];


int main(void) {

  printf("Measuring performance of Newhope, version: %-30s\n", NEWHOPE_VERSION);

  // initialize performance counter
  perf_initialize();

  // measure poly_uniform
  printf("%-40s", "poly_uniform with seed sampling");
  perf_measure(
    randombytes(seed, NEWHOPE_SEEDBYTES);
    poly_uniform(&sk_a, seed)
  );

  // measure poly_ntt
  printf("%-40s", "poly_ntt");
  perf_measure( poly_ntt(&sk_a) );

  // measure poly_invntt
  printf("%-40s", "poly_invntt");
  perf_measure( poly_invntt(&sk_a) );

  // measure poly_getnoise
  printf("%-40s", "poly_getnoise");
  perf_measure( poly_getnoise(&sk_a,seed,0) );

  // measure helprec
  printf("%-40s", "helprec");
  perf_measure( helprec(&sk_a, &sk_a, seed, 0) );

  // measure rec
  printf("%-40s", "rec");
  perf_measure( rec(key_a, &sk_a, &sk_a) );

  // measure newhope_keygen
  printf("%-40s", "newhope_keygen");
  perf_measure( newhope_keygen(senda+i*NEWHOPE_SENDABYTES, &sk_a) );

  // measure newhope_sharedb
  printf("%-40s", "newhope_sharedb");
  perf_measure( newhope_sharedb(key_b, sendb+i*NEWHOPE_SENDBBYTES, senda+i*NEWHOPE_SENDABYTES) );

  // measure newhope_shareda
  printf("%-40s", "newhope_shareda");
  perf_measure( newhope_shareda(key_a, &sk_a, sendb+i*NEWHOPE_SENDBBYTES) );

  printf("%s\n", "Components:");

  printf("%-40s", "mul_coefficients");
  perf_measure( mul_coefficients(coeffs, psis_bitrev_montgomery) );

  printf("%-40s", "bitrev_vector");
  perf_measure( bitrev_vector(coeffs) );

  printf("%-40s", "crypto_stream_chacha20");
  perf_measure( crypto_stream_chacha20((unsigned char *)buf,4*PARAM_N,n,seed) );

  // measure ntt
  printf("%-40s", "ntt");
  perf_measure( ntt(coeffs,omegas_montgomery) );

  // measure poly_add
  printf("%-40s", "poly_add");
  perf_measure( poly_add(&r, &a, &b) );

  // measure poly_pointwise
  printf("%-40s", "poly_pointwise");
  perf_measure( poly_pointwise(&r, &a, &b) );

  printf("%s\n", "NewHope-simple:");


  // measure poly_sub
  printf("%-40s", "poly_sub");
  perf_measure( poly_sub(&r, &a, &b) );

  // measure nhs_encode
  printf("%-40s", "nhs_encode");
  perf_measure( nhs_encode(&k, seed) );

  // measure nhs_decode
  printf("%-40s", "nhs_decode");
  perf_measure( nhs_decode(seed, &k) );

  // measure nhs_compress
  printf("%-40s", "nhs_compress");
  perf_measure( nhs_compress(comp, &c) );

  // measure nhs_decompress
  printf("%-40s", "nhs_decompress");
  perf_measure( nhs_decompress(&c, comp) );

  // measure newhope_simple_keygen
  printf("%-40s", "newhope_simple_keygen");
  perf_measure( newhope_simple_keygen(sendas+i*NEWHOPE_SIMPLE_SENDABYTES, &sk_a) );

  // measure newhope_simple_sharedb
  printf("%-40s", "newhope_simple_sharedb");
  perf_measure( newhope_simple_sharedb(key_b, sendbs+i*NEWHOPE_SIMPLE_SENDBBYTES, sendas+i*NEWHOPE_SIMPLE_SENDABYTES) );

  // measure newhope_simple_shareda
  printf("%-40s", "newhope_simple_shareda");
  perf_measure( newhope_simple_shareda(key_a, &sk_a, sendbs+i*NEWHOPE_SIMPLE_SENDBBYTES) );

  // terminate performance counter
  perf_terminate();
  printf("\n");

  return 0;
}
